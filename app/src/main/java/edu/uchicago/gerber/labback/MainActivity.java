package edu.uchicago.gerber.labback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    //define the views that have IDs in the layout
    private Button btnMain, btnSecond, btnThird;
    private View background;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //inflate
        setContentView(R.layout.activity_main);


        //get references to the objects on the heap
        btnMain = findViewById(R.id.btnMain);
        btnSecond = findViewById(R.id.btnSecond);
        btnThird = findViewById(R.id.btnThird);



        btnMain.setOnClickListener(this);

        btnSecond.setOnClickListener(this);

        btnThird.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        Button button;
        if (!(view instanceof Button))
            return;

        button = (Button)view;

        String strName = button.getText().toString();

        Intent intent;

        switch (strName){

            case "Main":
            default:

                intent = new Intent(MainActivity.this, MainActivity.class);

                break;

            case "Second":
                intent = new Intent(MainActivity.this, SecondActivity.class);
                break;


            case "Third":
                intent = new Intent(MainActivity.this, ThirdActivity.class);

                break;

        }

        startActivity(intent);
        finish();



    }





}
