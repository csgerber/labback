package edu.uchicago.gerber.labback;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ThirdActivity extends AppCompatActivity {

    //define the views that have IDs in the layout
    private Button btnMain, btnSecond, btnThird;
    private View background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //inflate
        setContentView(R.layout.activity_third);

        //get references to the objects on the heap
        btnMain = findViewById(R.id.btnMain);
        btnSecond = findViewById(R.id.btnSecond);
        btnThird = findViewById(R.id.btnThird);

        btnMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //define an intent and dispatch to the operating sytstem
                Intent intent = new Intent(ThirdActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btnSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //define an intent and dispatch to the operating sytstem
                Intent intent = new Intent(ThirdActivity.this, SecondActivity.class);
                startActivity(intent);
                finish();

            }
        });


        btnThird.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //define an intent and dispatch to the operating sytstem
                Intent intent = new Intent(ThirdActivity.this, ThirdActivity.class);
                startActivity(intent);
                finish();

            }
        });


    }
}
